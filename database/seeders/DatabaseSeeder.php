<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \App\Models\Expense::factory(35)->create();
        $clientsIds = \App\Models\Client::factory(100)->create()->pluck('id')->toArray();
        $employeesIds = \App\Models\Employee::factory(50)->create()->pluck('id')->toArray();
        $orders = \App\Models\Order::factory(3000)->make()->each(function ($order) use ($clientsIds, $employeesIds) {
            $order->client_id = array_rand($clientsIds);
            $order->employee_id = array_rand($employeesIds);
            $order->order_size = mt_rand(10, 1000);
        })->toArray();

        \App\Models\Order::insert($orders);
    }
}
