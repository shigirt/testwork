<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'size',
    ];

    /**
     * @return mixed
     */
    public static function getExpanse(\DateTime  $start, \DateTime $end)
    {
        return self::whereDate('updated_at', '>=', $start)
            ->whereDate('updated_at', '<=', $end)
            ->sum('size');
    }
}
