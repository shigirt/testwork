<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory, Notifiable;

    /**
     * @return mixed
     */
    public static function getSumOrders(\DateTime  $start, \DateTime $end)
    {
        return self::whereDate('updated_at', '>=', $start)
            ->whereDate('updated_at', '<=', $end)
            ->sum('order_size');
    }
}
