<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fio',
        'salary',
    ];

    /**
     * @param \DateTime|null $start
     * @param \DateTime|null $end
     * @return HasMany
     */
    public function orders(?\DateTime  $start = null, ?\DateTime $end = null):HasMany
    {
        $tmp = $this->hasMany(Order::class);
        if (null !== $start && null !== $end) {
            $tmp->whereDate('updated_at', '>=', $start)
              ->whereDate('updated_at', '<=', $end);
        }

        return $tmp;
    }


    public static function getSalaries(?\DateTime  $start = null, ?\DateTime $end = null)
    {
        $tmp =  self::with(['orders' => function ($query) use ($start, $end) {
            $query->whereDate('updated_at', '>=', $start)
                ->whereDate('updated_at', '<=', $end);
        } ]);
        if (null !== $start && null !== $end) {
            $tmp->whereDate('updated_at', '>=', $start)
                ->whereDate('updated_at', '<=', $end);
        }
        return $tmp->get();
    }
}
