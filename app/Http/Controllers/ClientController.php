<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use App\Models\Order;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public static function getProfit(string $start, string $end)
    {
        try {
            $otherExpanse = Expense::getExpanse(new \DateTime($start), new \DateTime($end));
            $salaryExpanse = EmploeeController::getSalaries($start, $end)->sum('salary');
            $rev = Order::getSumOrders(new \DateTime($start), new \DateTime($end));
            return [
                'start_time' => $start,
                'end_time' => $end,
                'profit' => $rev - ($otherExpanse + $salaryExpanse),
                'income' => $rev,
            ];
        } catch (\Exception $ex) {
            return [
                'err'=>$ex->getMessage(),
            ];
        }
    }
}
