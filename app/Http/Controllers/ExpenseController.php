<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    /**
     * @param string $start
     * @param string $end
     * @return array
     * @throws \Exception
     */
    public static function getExpense(string $start, string $end)
    {
        try {
            $otherExpanse = Expense::getExpanse(new \DateTime($start), new \DateTime($end));
            $salaryExpanse = EmploeeController::getSalaries($start, $end)->sum('salary');
            return [
                'start_time' => $start,
                'end_time' => $end,
                'other_expanse' => $otherExpanse,
                'salary_expanse' => $salaryExpanse,
                'sum_expanse' => $otherExpanse + $salaryExpanse,
            ];
        } catch (Exception $ex) {
            return [
                'err'=>$ex->getMessage(),
            ];
        }
    }
}
