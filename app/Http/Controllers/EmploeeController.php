<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Expense;
use Illuminate\Http\Request;

class EmploeeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public static function getSalaries(?string $start = null, ?string $end = null)
    {
        $tmp = Employee::getSalaries(new \DateTime($start), new \DateTime($end));
        if (count($tmp) > 0) {
            $best = [0, 0];
            foreach ($tmp as $key => $employee) {
                foreach ($employee['orders'] as $order) {
                    $tmp[$key]['salary'] += (float)config('app.percent') / 100 * $order['order_size'];
                }
                $tmp[$key]['salary'] = round($tmp[$key]['salary'], 2);
                if ($employee['salary'] > $best[0]) {
                    $best[0] = $employee['salary'];
                    $best[1] = $key;
                }
                unset($tmp[$key]['orders']);
            }
            $tmp[$best[1]]['salary'] += (float)config('app.bonus');
        }

        return $tmp;
    }
}
